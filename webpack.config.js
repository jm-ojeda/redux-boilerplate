var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

const ROOT_PATH = path.resolve(__dirname);

const babelSettings = {
  presets: ['react','es2015']
};

module.exports ={
  entry: path.resolve(ROOT_PATH, "src/app.js"),
  output:{
    path: path.resolve(ROOT_PATH, 'public'),  // This is where images AND js will go
    publicPath: '/',  // This is used to generate URLs to e.g. images
    filename: 'bundle.js'
  },
  module: {
    loaders:[
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loaders: ['react-hot', 'babel-loader?'+JSON.stringify(babelSettings)],
      },
      {
          test: /\.scss$/,
          loader: ExtractTextPlugin.extract(
            "style",
            "css!sass")
      },
      { // inline base64 URLs for <=8k images, direct URLs for the rest
        test: /\.(png|jpg)$/,
        loader: 'url-loader?limit=8192'
      }
    ]
  },
  devServer: {
    contentBase: path.resolve(ROOT_PATH, 'public'),
    historyApiFallback: true,
    hot: true,
    inline: true,
    progress: true
  },
  devtool: 'inline-source-map',
  plugins: [
    new ExtractTextPlugin('style.css'),
    new webpack.HotModuleReplacementPlugin()
]
};
